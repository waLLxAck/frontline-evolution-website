const faunadb = require('faunadb');

const q = faunadb.query;
const client = new faunadb.Client({
    secret: process.env.FAUNA_SERVER_SECRET,
});

exports.handler = async (event, context) => {
    if (!event.body) {
        return {
            statusCode: 400,
            body: "No data received",
        };
    }

    const payload = new URLSearchParams(event.body);
    const email = payload.get('email');

    if (!email) {
        return {
            statusCode: 400,
            body: "Email is required",
        };
    }

    return client.query(q.Create(q.Collection('EmailList'), { data: { email } }))
        .then((response) => {
            return {
                statusCode: 200,
                body: JSON.stringify(response),
            };
        })
        .catch((error) => {
            return {
                statusCode: 400,
                body: JSON.stringify(error),
            };
        });
};
