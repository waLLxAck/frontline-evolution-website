document.addEventListener('DOMContentLoaded', () => {
    // Existing code
    const year = new Date().getFullYear();
    document.getElementById("year").innerText = year;

    const successToast = document.getElementById('toast-success');
    const errorToast = document.getElementById('toast-error');

    // Function to show a toast
    const showToast = (toastElem) => {
        toastElem.classList.remove('animate__animated', 'animate__zoomInUp', 'animate__zoomOutDown');
        toastElem.classList.add('animate__animated', 'animate__zoomInUp');
        toastElem.classList.remove('hidden', 'translate-y-full');
        toastElem.classList.add('translate-y-0');

        setTimeout(() => {
            toastElem.classList.remove('animate__zoomInUp');
            toastElem.classList.add('animate__zoomOutDown');
            setTimeout(() => {
                toastElem.classList.add('hidden', 'translate-y-full');
                toastElem.classList.remove('translate-y-0');
            }, 1000);
        }, 3000);
    };

    const form = document.querySelector('form[name="waitlist"]');
    const joinButton = document.querySelector('button[type="submit"]');

    form.addEventListener('submit', async (event) => {
        event.preventDefault();
        joinButton.classList.add('animate__animated', 'animate__zoomOutDown');

        const formData = new FormData(form);
        const email = formData.get('email');

        // Find the new email container
        const emailContainer = document.getElementById('email-container');
        const emailInput = document.getElementById('email');

        try {
            const response = await fetch('/.netlify/functions/store-email', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: `email=${encodeURIComponent(email)}`
            });

            const data = await response.json();

            if (response.ok) {
                showToast(successToast);

                // Now add animations for the emailContainer, after the fetch operation
                emailContainer.classList.add('animate__animated', 'animate__zoomOutDown'); // Zoom-out

                setTimeout(() => {
                    // Clear email field
                    emailInput.value = '';
                    emailContainer.classList.remove('animate__zoomOutDown');

                    // Add zoom-in animation
                    emailContainer.classList.add('animate__zoomInUp');

                    setTimeout(() => {
                        emailContainer.classList.remove('animate__zoomInUp');
                    }, 1000); // Zoom-in animation duration

                }, 1000); // Zoom-out animation duration

            } else {
                showToast(errorToast);
            }

        } catch (error) {
            console.error("An error occurred:", error);
            showToast(errorToast);
        }

        setTimeout(() => {
            joinButton.classList.remove('animate__zoomOutDown');
            joinButton.classList.add('animate__zoomInUp');

            setTimeout(() => {
                joinButton.classList.remove('animate__zoomInUp');
                joinButton.blur();
            }, 1000); // Zoom-in animation duration

        }, 1000);  // Zoom-out animation duration

    });
});
